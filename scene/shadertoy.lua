--[[ shadertoy1.lua

    Takes some basic raymarching code from shadertoy and applies the incoming
    view and projection matrices so raymarched objects can coexist with
    rasterized ones by sharing a color and depth buffer.
]]
shadertoy1 = {}

shadertoy1.__index = shadertoy1

function shadertoy1.new(...)
    local self = setmetatable({}, shadertoy1)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function shadertoy1:init()
    self.shader = nil
    self.time = 0
    self.gridThick = 1
    self.sunStripeThick = 1
    self.skylineHeightScale = 1
	self.gridScroll = 0
end

local ffi = require("ffi")
require("util.fullscreen_shader")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local rm_frag = [[
#line 32
uniform mat4 mvmtx;
uniform mat4 prmtx;
uniform float iTime;
uniform vec2 iResolution;

uniform float uGridThick;
uniform float uSunStripeThick;
uniform float uSkylineHeightScale;

#define NONE  -1
#define PLANE  0
#define SPHERE 1

#define TMAX 10000.


const vec4 skyTop = vec4(0.15f, 0.045f, 0.25f, 1.0f);
const vec4 skyBottom = vec4(0.60f, 0.18f, 0.996f, 1.0f);

// see https://www.iquilezles.org/www/articles/checkerfiltering/checkerfiltering.htm
float gridTextureGradBoxFilter(vec2 uv, vec2 ddx, vec2 ddy)
{
float N = 50.0 - 40.*uGridThick;
    uv += 0.5f;
    vec2 w = max(abs(ddx), abs(ddy)) + 0.01;
    vec2 a = uv + 0.5 * w;
    vec2 b = uv - 0.5 * w;

    vec2 i = (floor(a) + min(fract(a) * N, 1.0) -
        floor(b) - min(fract(b) * N, 1.0)) / (N * w);

    return (1.0 - i.x) * (1.0 - i.y);
}

// Generate color based on object uv and position
float gridTexture(vec2 uv)
{
float N = 50.0 - 40.*uGridThick;
    uv += 0.5f;
    vec2 i = step(fract(uv), vec2(1.0 / N, 1.0 / N));
    return (1.0 - i.x) * (1.0 - i.y);
}


// Calculate uv coordinate of object
vec2 texCoords(vec3 pos, int objectType)
{
    vec2 uv;
    if (objectType == PLANE)
    {
        uv = pos.xz;
    }
    else if (objectType == SPHERE)
    {
        // Todo
    }

    uv.y -= iTime * 10.;
    return 0.5 * uv;
}

// Check if ray intersects with an object and return position, distance along ray, normal and object type
float traceRay(vec3 rayOrigin, vec3 rayDir, inout vec3 pos, inout vec3 nor, inout int objType)
{
    float tmin = TMAX;
    pos = vec3(0.0f, 0.0f, 0.0f);
    nor = vec3(0.0f, 0.0f, 0.0f);
    objType = NONE;

    // Raytrace plane
    // ray plane intersection, since normal is (0, 1, 0) only y component matters
    // simplified version of 
    // float t = -dot(rayOrigin-0.01, nor)/dot(rayDir, nor);
    float t = (-1.0 - rayOrigin.y) / rayDir.y;

    if (t > 0.0)
    {
        tmin = t;
        nor = vec3(0.0f, 1.0f, 0.0f);
        pos = rayOrigin + rayDir * t;
        objType = PLANE;
    }

    return tmin;
}

// Creates ray based on camera position
void createRay(in vec2 pixel, inout vec3 rayOrigin, inout vec3 rayDirection)
{
    // Remap input position into ndc space in range -1..1
    vec2 p = (2. * pixel.xy - iResolution.xy) / iResolution.y;

    vec3 camPos = vec3(0.0f, 1.0f, 5.0f);
    vec3 camDir = vec3(0.0f, 1.0f, 0.0f);

    // Create look-at matrix with Gram–Schmidt process
    vec3 dir   = normalize(camDir - camPos);
    vec3 right = normalize(cross(dir, vec3(0.0f, 1.0f, 0.0f)));
    vec3 up    = normalize(cross(right, dir));

    // View ray
    rayDirection = normalize(p.x * right + p.y * up + 2.0f * dir);
    rayOrigin = camPos;

    // Apply modelview matrix
    vec3 ro = -mvmtx[3].xyz;
    rayOrigin += ro;
    rayOrigin *= mat3(mvmtx);
    rayDirection *= mat3(mvmtx);
}

float rand(float x)
{
    return fract(sin(x) * 100000.0f);
}

void mainImage(out vec4 fragColor, vec2 fragCoord)
{
    // Invert y
    // Remap input position into ndc space in range -1..1
    vec2 p = (-iResolution.xy + 2.0*fragCoord) / iResolution.y;

    float radius = 0.5f;
    vec2 ctr = vec2(0.0f, 0.3f);
    vec2 diff = p - ctr;

    // get random heights of buildings
    float width = 50.0;
    float skylineHeight = rand((mod(trunc(p.x* width), width)));
    skylineHeight *= uSkylineHeightScale;

    // have them get smaller along the edges
    float falloffFactor = 0.7;
    skylineHeight *= 1.0 - abs(p.x) * falloffFactor;


    float t = TMAX;
    if (p.y > 0.0 && p.y * 2.7 < skylineHeight)
    {
        fragColor = vec4(0.1, 0.1, 0.1, 1.0);
        fragColor *= rand(p.y) * rand(p.x) * 3.0f;
        t = 50.0f;
    }
    else if (dot(diff, diff) < (radius * radius) && p.y > 0.0f)
    {
        // Sun color
        float nStripes = 15.f;
        float th = 1.f - uSunStripeThick;
        fragColor = vec4(0.97f, 0.46f, 0.3f, 1.0f)
            * step(
                fract(p.y * nStripes)
                - p.y/5., th);
        t = 10.0;
    }
    else
    {
        // Sky
        fragColor = mix(skyTop, skyBottom, .5 - p.y/2.);
    }

    vec3 rayDir;
    vec3 rayOrigin;
    vec3 rayOriginDdx;
    vec3 rayDirDdx;
    vec3 rayOriginDdy;
    vec3 rayDirDdy;

    // Create main ray and rays for partial derivatives basically one pixel to right and one pixel down
    createRay(fragCoord, rayOrigin, rayDir);
    createRay(fragCoord + vec2(1.0, 0.0), rayOriginDdx, rayDirDdx);
    createRay(fragCoord + vec2(0.0, 1.0), rayOriginDdy, rayDirDdy);

    // Raytrace
    vec3 pos;
    vec3 nor;
    int objectType = NONE;

    float groundt = traceRay(rayOrigin, rayDir, pos, nor, objectType);

    t = min(groundt, t);

    // compute ray differentials, intersect ray with tangent plane to the surface
    //
    // Take the new position and subtract the hit position from original camera ray.
    // This will give us a ray from the position to the new ddx/ddy origin. Then
    // we take a dot product with the normal which projects the ray in the direction
    // of the normal. Basically it takes the component of the ray that is parallel
    // to the normal. Then we do the same with the ray direction and the normal and 
    // this gives us a small amount, basically the amount of ray in the direction of 
    // the same normal. Then we divide the ddx/y - position projection with the new ray
    // and we get the amnt we have to multiply to reach the new ddx pos on the tangent
    // plane.
    vec3 posDdx = rayOriginDdx - rayDirDdx * dot(rayOriginDdx - pos, nor) / dot(rayDirDdx, nor);
    vec3 posDdy = rayOriginDdy - rayDirDdy * dot(rayOriginDdy - pos, nor) / dot(rayDirDdy, nor);

    // Calculate uv coords
    vec2 uv = texCoords(pos, objectType);

    // Texture diffs
    vec2 uvDdx = texCoords(posDdx, objectType) - uv;
    vec2 uvDdy = texCoords(posDdy, objectType) - uv;

    if (objectType == PLANE)
    {
        float color = gridTextureGradBoxFilter(uv, uvDdx, uvDdy);
        fragColor = mix(vec4(217.0 / 255.0, 117.0 / 255.0, 217.0 / 255.0, 1.0f), vec4(133.0 / 255.0, 46.0 / 255.0, 106.0 / 255.0, 1.0f), color);
    }

    // fog
    if (t < TMAX)
    {
        fragColor = mix(fragColor, skyBottom, 1.0 - exp(-0.0001 * t * t));
    }
    return;
}

void main()
{
    mainImage(fragColor, uv * iResolution);
}
]]

function shadertoy1:initGL()
    self.shader = FullscreenShader.new(rm_frag)
    self.shader:initGL()
end

function shadertoy1:exitGL()
    self.shader:exitGL()
end

function shadertoy1:render_for_one_eye(view, proj)
    local function set_variables(prog)
        local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
        gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
        local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
        gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

        local ut_loc = gl.glGetUniformLocation(prog, "iTime")
        gl.glUniform1f(ut_loc, self.gridScroll)
        local ur_loc = gl.glGetUniformLocation(prog, "iResolution")
        gl.glUniform2f(ur_loc, 1600, 1200)

        local gt_loc = gl.glGetUniformLocation(prog, "uGridThick")
        gl.glUniform1f(gt_loc, self.gridThick)

        local ss_loc = gl.glGetUniformLocation(prog, "uSunStripeThick")
        gl.glUniform1f(ss_loc, self.sunStripeThick)
        local sk_loc = gl.glGetUniformLocation(prog, "uSkylineHeightScale")
        gl.glUniform1f(sk_loc, self.skylineHeightScale)
    end

    gl.glDisable(GL.GL_DEPTH_TEST)
    self.shader:render(view, proj, set_variables)
end

function shadertoy1:timestep(absTime, dt)
    self.time = absTime
	--print(self.time)
	if self.time > 15 then
		self.gridScroll = self.time
	end
end

return shadertoy1
