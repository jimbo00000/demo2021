--[[ particles_scene.lua

]]
particles = {}

particles.__index = particles

function particles.new(...)
    local self = setmetatable({}, particles)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function particles:init()
    self.vbos = {}
    self.vao = 0
    self.progs = {}
    self.numPoints = 1024 --*1024
    self.time = 0
    self.lightData = {}
end

--require("util.objfile")
local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

--[[
    Drawing instanced cubes: simple color per-vertex.
]]
local per_vertex_color_vert = [[
#version 330

in vec4 vPosition;
in vec4 vNormal;
in vec2 vUV;
in vec4 instancePosition;
in vec4 instanceOrientation;

out vec3 vfWorldPos;
out vec3 vfNormal;
out vec2 vfUV;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;
uniform float instScale;

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

void main()
{
    vec3 pos = vPosition.xyz;
    vec4 q = normalize(instanceOrientation);
    pos = qtransform(q, pos);
    pos *= instScale;
    pos += instancePosition.xyz;
    vfWorldPos = (mmtx * vec4(pos,1.)).xyz;
    vfNormal = normalize((mat3(mmtx)) * qtransform(q,vNormal.xyz));
    vfUV = vUV;
    gl_Position = prmtx * vmtx * mmtx * vec4(pos, 1.);
}
]]

local per_vertex_color_frag = [[
#version 330

in vec3 vfWorldPos;
in vec3 vfNormal;
in vec2 vfUV;
out vec4 fragColor;

#line 84
uniform float iTime;
//uniform vec2 iResolution;
const vec2 iResolution = vec2(500, 500);;

// raymarched @party logo
const mat4 modelMtx = mat4(1.);
uniform mat4 viewMtx;
uniform mat4 projMtx;


// https://www.shadertoy.com/view/Mll3W2
//#define PARTY_ON

#define t (3.*iTime)
float party = 0.;

// math
const float PI = 3.14159265359;
const float DEG_TO_RAD = PI / 180.0;
mat3 rotationX(float r) {
    float ct=cos(r), st=sin(r);
    return mat3(1., 0.,  0.,  0., ct, -st,  0., st,  ct);}
mat3 rotationY(float r) {
    float ct=cos(r), st=sin(r);
    return mat3(ct, 0., st,  0., 1., 0.,  -st, 0., ct);}
mat3 rotationZ(float r) {
    float ct=cos(r), st=sin(r);
    return mat3(ct, -st, 0.,  st, ct, 0.,  0., 0., 1.);}
mat3 rotationXY(vec2 angle) {
    vec2 c = cos(angle);
    vec2 s = sin(angle);
    return mat3(
        c.y    ,  0.0, -s.y,
        s.y*s.x,  c.x,  c.y*s.x,
        s.y*c.x, -s.x,  c.y*c.x);
}


// libiq

// exponential smooth min (k = 32);
float smine( float a, float b, float k )
{
    float res = exp( -k*a ) + exp( -k*b );
    return -log( res )/k;
}

// polynomial smooth min (k = 0.1);
float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

////////////// DISTANCE FUNCTIONS
//
// Primitives from http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
//
float udBox( vec3 p, vec3 b )
{
  return length(max(abs(p)-b,0.0));
}
float sdBox( vec3 p, vec3 b )
{
  vec3 d = abs(p) - b;
  return min(max(d.x,max(d.y,d.z)),0.0) +
         length(max(d,0.0));
}
float udRoundBox( vec3 p, vec3 b, float r )
{
    return length(max(abs(p)-b,0.0))-r;
}
float sdPlane( vec3 p, vec4 n )
{
    // n must be normalized
    return dot(p,n.xyz) + n.w;
}

//
// Composites
//
float rollprism(vec3 pos)
{
    return max(
        udRoundBox(pos, vec3(2.,1.4,5.), 3.5),
        -sdPlane(pos, vec4(normalize(vec3(-1.,0.,0.)),3.))
        );
}

float rollprism2(vec3 pos)
{
    return max(
        udRoundBox(pos, vec3(2.,2.,5.), 7.5),
        -udRoundBox(pos, vec3(2.,2.,15.), 5.5)
        );
}

float atsign(vec3 pos)
{
    float d = min(
        rollprism(pos-vec3(1.,0.,0.)),
        rollprism2(pos)
        );
    d = smin(d,
             sdBox(pos-vec3(4.2,-3.5,0.), vec3(3.5,1.4,5.))
            ,1.);    
    d = max(d, -sdBox(pos-vec3(6.,-8.4,0.), vec3(6.,3.5,12.))); // chop horiz
    
    // chop off front and back
    float w = 0.;//1.+sin(t);
    d = max(d, -sdPlane(pos                 , vec4(vec3(0.,0.,-1.),0.)));
    d = max(d, -sdPlane(pos-vec3(0.,0.,-1.5-w), vec4(vec3(0.,0.,1.),0.)));
    return d;
}

float attail(vec3 pos)
{
    float s = -0.+0.5*pow((1.+0.1*pos.x),1.7);
    return sdBox(pos, vec3(5.,0.8+s,.6+s));
}

float at(vec3 pos)
{
    // dance
    float p = 0.02*party;
    pos = rotationY(p*pos.y*sin(3.*t)) * pos;
    pos = rotationX(p*pos.y*sin(5.*t)) * pos;
    
    return min(
        atsign(pos),
        attail(
            rotationY(-.03*(pos.x+7.)) *
            (pos-vec3(3.,-8.5,.0)))
        );
}

float DE_atlogo( vec3 pos )
{
    mat3 rotmtx = mat3(modelMtx);//1.);//getMouseRotMtx();
    pos = rotmtx * pos;
    float d2 = 9999.;
    return min(d2, at(pos));
}


//
// lighting and shading
//
vec4 shading( vec3 v, vec3 n, vec3 eye ) {
    float shininess = 16.0;
    vec3 ev = normalize( v - eye );
    vec3 ref_ev = reflect( ev, n ); 
    vec4 final = vec4( 0.0 );
    // disco light
    {
        vec3 light_pos   = vec3( 1.0, 10.0, 30.0 );
        float p = party;
        vec3 light_color = vec3(p*sin(3.*t), p*sin(3.3*t), p*sin(4.*t));
        vec3 vl = normalize( light_pos - v );
    
        float diffuse  = 0.;//max( 0.0, dot( vl, n ) );
        float specular = max( 0.0, dot( vl, ref_ev ) );
        specular = pow( specular, shininess );
        
        final.xyz += light_color * ( diffuse + specular ); 
        final.w = 1.;
    }
    
    // white light
    {
        vec3 light_pos   = vec3( 10.0, 20.0, -10.0 );
        //vec3 light_pos   = vec3( -20.0, -20.0, -20.0 );
        vec3 light_color = vec3( 1.);//0.3, 0.7, 1.0 );
        vec3 vl = normalize( light_pos - v );
    
        shininess = 8.;
        float diffuse  = 0.;//max( 0.0, dot( vl, n ) );
        float specular = max( 0.0, dot( vl, ref_ev ) );
        specular = pow( specular, shininess );
        
        final.xyz += light_color * ( diffuse + specular ); 
    }
        final.w = 1.;

    return final;
}

// get gradient in the world
vec3 gradient( vec3 pos ) {
    const float grad_step = 0.31;
    const vec3 dx = vec3( grad_step, 0.0, 0.0 );
    const vec3 dy = vec3( 0.0, grad_step, 0.0 );
    const vec3 dz = vec3( 0.0, 0.0, grad_step );
    return normalize (
        vec3(
            DE_atlogo( pos + dx ) - DE_atlogo( pos - dx ),
            DE_atlogo( pos + dy ) - DE_atlogo( pos - dy ),
            DE_atlogo( pos + dz ) - DE_atlogo( pos - dz )   
        )
    );
}

// ray marching
float ray_marching( vec3 origin, vec3 dir, float start, float end ) {
    const int max_iterations = 255;
    const float stop_threshold = 0.001;
    float depth = start;
    for ( int i = 0; i < max_iterations; i++ ) {
        float dist = DE_atlogo( origin + dir * depth );
        if ( dist < stop_threshold ) {
            return depth;
        }
        depth += dist;
        if ( depth >= end) {
            return end;
        }
    }
    return end;
}

// get ray direction from pixel position
vec3 ray_dir( float fov, vec2 xy ) {
    float cot_half_fov = tan( ( 90.0 - fov * 0.5 ) * DEG_TO_RAD );  
    float z = 1. * cot_half_fov;
    
    return normalize( vec3( xy, -z ) );
}

vec4 getSceneColor_atlogo( in vec3 ro, in vec3 rd )
{
    const float clip_far = 100.0;
    float depth = ray_marching( ro, rd, -10.0, clip_far );
    if ( depth >= clip_far ) {
        return vec4(.0);
    }
    vec3 pos = ro + rd * depth;
    vec3 n = gradient( pos );
    return shading( pos, n, ro );
}


//
// The cube
//
mat3 getCubeMtx()
{
    return rotationY(.8+party*pow(abs(sin(PI*2.*iTime)),5.)) * rotationX(.3);
}

float DE_cube( vec3 pos )
{
    pos = getCubeMtx() * mat3(modelMtx) * pos;
    return udRoundBox(pos, vec3(1.5), .15);
}

vec4 shade_cube(vec3 v, vec3 n, vec3 ntx, vec3 eye) {
    vec3 ev = normalize(v - eye);
    vec4 final = vec4(0.);
    vec3 light_pos = vec3(-10.,20.,40.);
    vec3 vl = normalize(light_pos - v);
    float diffuse = max(0.0, dot( vl, n ));
    final.xyz += 1.3 * diffuse; 
    // transform normals with the cube to find flat faces/edges
    float px = abs(dot(ntx,vec3(1.,0.,0.)));
    float py = abs(dot(ntx,vec3(0.,1.,0.)));
    float pz = abs(dot(ntx,vec3(0.,0.,1.)));
    float p = max(px,max(py,pz));
    final.xyz *= smoothstep(0.9,1.,length(p));
    final.w = 1.;
    return final;
}

vec3 grad_cube(vec3 pos) {
    const float gs = 0.02;
    const vec3 dx = vec3(gs, 0., 0.);
    const vec3 dy = vec3(0., gs, 0.);
    const vec3 dz = vec3(0., 0., gs);
    return normalize( vec3(
            DE_cube(pos + dx) - DE_cube(pos - dx),
            DE_cube(pos + dy) - DE_cube(pos - dy),
            DE_cube(pos + dz) - DE_cube(pos - dz)   
        ));
}

float raymarch_cube(vec3 origin, vec3 dir, float start, float end) {
    const int max_iterations = 64;
    const float stop_threshold = 0.01;
    float depth = start;
    for (int i=0; i<max_iterations; i++) {
        float dist = DE_cube(origin + dir*depth);
        if (dist < stop_threshold) return depth;
        depth += dist;
        if (depth >= end) return end;
    }
    return end;
}

vec4 getCubeColor(in vec3 ro, in vec3 rd) {
    const float clip_far = 100.0;
    float depth = raymarch_cube(ro, rd, 0., clip_far);
    if ( depth >= clip_far )
        return getSceneColor_atlogo(ro,rd);
    vec3 pos = ro + rd * depth;
    vec3 ne = grad_cube(pos);
    vec3 ntx = getCubeMtx() * ne;
    return shade_cube(pos, ne, ntx, ro);
}

///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = //inverse(projMtx) * 
        ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
#if 0 //def PARTY_ON
    party = 1.;
#endif

    vec2 uv = vfUV;
    vec2 uv11 = uv * 2.0 - vec2(1.0);
    vec3 ro = vec3(0.);// getEyePoint(viewMtx);
    vec3 rd = getRayDirection(uv11);

    ro.z += 14.;
    //ro *= mat3(viewMtx);
    //rd *= mat3(viewMtx);

    fragColor = getCubeColor(ro, rd);
    //fragColor = vec4(uv11, 0.,1.);
}


]]

function particles:setDataDirectory(dir)
    self.data_dir = dir
end

function particles:loadModel(filename)
    self.obj = objfile.new()
    if self.data_dir then filename = self.data_dir .. "/" .. filename end
    print("Loading obj "..filename)
    self.obj:loadmodel(filename)
    local str = (#self.obj.vertlist/4).." vertices*3, "
    str = str..(#self.obj.normlist/4).." normals*3, "
    str = str..(#self.obj.texlist/4).." texcoords*3, "
    str = str..(#self.obj.idxlist/4).." ints*3 for triangle indices."
    print(str)

    -- Compute aabb of obj
    if true then
        local nv = #self.obj.vertlist/3
        local aabbmin = {}
        local LARGEVAL = 999999999
        self.obj.aabb = {
            min={LARGEVAL,LARGEVAL,LARGEVAL,},
            max={-LARGEVAL,-LARGEVAL,-LARGEVAL,},
        }
        for i=1,nv-1 do
            local vo = 3*i
            for j=1,3 do
                self.obj.aabb.min[j] = math.min(self.obj.aabb.min[j], self.obj.vertlist[vo+j-1])
                self.obj.aabb.max[j] = math.max(self.obj.aabb.max[j], self.obj.vertlist[vo+j-1])
            end
        end
        print("AABB min: ",
            self.obj.aabb.min[1],
            self.obj.aabb.min[2],
            self.obj.aabb.min[3]
            )
        print("AABB max: ",
            self.obj.aabb.max[1],
            self.obj.aabb.max[2],
            self.obj.aabb.max[3]
            )
        self.objscale = 0
        for j=1,3 do
            self.objscale = math.max(self.objscale, self.obj.aabb.max[j]-self.obj.aabb.min[j])
        end
        print("Obj scale: ",self.objscale)
    end
end

function particles:initModelAttributes()
    --print("Number of objects: "..#self.obj.olist)
    if #self.obj.olist == 0 then return end

    local v = self.obj.vertlist
    local n = self.obj.normlist
    local i = self.obj.idxlist

    local verts = glFloatv(#v,v)
    local norms = glFloatv(#n,n)

    local prog = self.progs.color
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.originalTris = vvbo

    gl.glEnableVertexAttribArray(vpos_loc)

    local vnorm_loc = gl.glGetAttribLocation(prog, "vNormal")
    if vnorm_loc > -1 then
        local nvbo = glIntv(0)
        gl.glGenBuffers(1, nvbo)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(norms), norms, GL.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(vnorm_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
        self.vbos.originalNorms = nvbo

        gl.glEnableVertexAttribArray(vnorm_loc)
    end

    local lines = glUintv(#i,i)
    local ivbo = glIntv(0)
    gl.glGenBuffers(1, ivbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, ivbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(lines), lines, GL.GL_STATIC_DRAW)
    self.vbos.triIndices = ivbo
end

-- A simple cube mesh with per-vertex colors.
function particles:init_single_instance_attributes()
    self:init_single_instance_attributes_from_array()
    --self:init_single_instance_attributes_from_obj("sphere2")
end

function particles:init_single_instance_attributes_from_array()
    local verts = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,

        0,0,1,
        1,0,1,
        1,1,1,
        0,1,1,

        0,0,0,
        1,0,0,
        1,0,1,
        0,0,1,

        0,1,0,
        1,1,0,
        1,1,1,
        0,1,1,

        0,0,0,
        0,1,0,
        0,1,1,
        0,0,1,

        1,0,0,
        1,1,0,
        1,1,1,
        1,0,1,
    }
    for i=1,#verts do verts[i] = verts[i]-.5 end
    local vertsArray = glFloatv(#verts, verts)

    local norms = {
        0,0,-1,
        0,0,-1,
        0,0,-1,
        0,0,-1,

        0,0,1,
        0,0,1,
        0,0,1,
        0,0,1,

        0,-1,0,
        0,-1,0,
        0,-1,0,
        0,-1,0,

        0,1,0,
        0,1,0,
        0,1,0,
        0,1,0,

        -1,0,0,
        -1,0,0,
        -1,0,0,
        -1,0,0,

        1,0,0,
        1,0,0,
        1,0,0,
        1,0,0,
    }
    local normsArray = glFloatv(#norms, norms)


    local uvs = {
        0,0,
        1,0,
        1,1,
        0,1,

        0,0,
        1,0,
        1,1,
        0,1,

        0,0,
        1,0,
        1,1,
        0,1,

        0,0,
        1,0,
        1,1,
        0,1,

        0,0,
        1,0,
        1,1,
        0,1,

        0,0,
        1,0,
        1,1,
        0,1,
    }
    local uvsArray = glFloatv(#uvs, uvs)

    local prog = self.progs.color
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vNormal")
    local vuv_loc = gl.glGetAttribLocation(prog, "vUV")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(vertsArray), vertsArray, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(normsArray), normsArray, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    local uvbo = glIntv(0)
    gl.glGenBuffers(1, uvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, uvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(uvsArray), uvsArray, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vuv_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, uvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
    gl.glEnableVertexAttribArray(vuv_loc)

    local tris = {
        0,3,2, 1,0,2,
        4,5,6, 7,4,6,

        8,11,10, 9,8,10,
        12,13,14, 15,12,14,

        16,19,18, 17,16,18,
        20,21,22, 23,20,22,
    }
    local trisArray = glUintv(#tris, tris)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(trisArray), trisArray, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)

    self.numInstTris = #tris
end

--[[
"cube",
"tetrahedron",
"octahedron",
"sphere",
"sphere2",
"Biplane2",
"NYERSEY_",
"Submarine2",
"hull22",
]]
function particles:init_single_instance_attributes_from_obj(objname)
    self:loadModel("models/"..objname..".obj")
    self:initModelAttributes()
    self.numInstTris = #self.obj.idxlist
end

--[[
    Scatter instance positions randomly in particles array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numPoints;

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;
    float fi = float(index+1) / float(numPoints);

    positions[index] = vec4(hash(fi*2.3), hash(fi*4.1), hash(fi*5.3), 1.);

    orientations[index] = vec4(hash(fi*4.3), hash(fi*2.9), hash(fi*7.), hash(fi*4.7));

    vec3 randv = vec3(hash(fi*4.9), hash(fi*1.9), hash(fi*3.7));
    vec3 motiondir = 2.*(vec3(-.5) + randv); // isotropic, go in all directions
    motiondir = pow(length(motiondir), 7.) * normalize(motiondir);
    velocities[index] = vec4(.05*motiondir, 0.);

    vec3 randav = vec3(hash(fi*6.1), hash(fi*5.9), hash(fi*3.1));
    vec3 rotangle = 2.*(vec3(-.5) + randav); // isotropic, go in all directions
    rotangle = pow(rotangle, vec3(13.));
    angularVelocities[index] = normalize(vec4(rotangle, 30.));
}
]]
function particles:randomize_instance_positions()
    if self.progs.scatter_instances == nil then
        self.progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end
    local pvbo = self.vbos.inst_positions
    local ovbo = self.vbos.inst_orientations
    local vvbo = self.vbos.inst_velocities
    local avvbo = self.vbos.inst_angularvelocities
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    local prog = self.progs.scatter_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numPoints"), self.numPoints)
    gl.glDispatchCompute(self.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Apply angular velocity every timestep
]]
local instance_timestep_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numPoints;
uniform float dt;

// http://www.gamedev.net/reference/articles/article1095.asp
//w = w1w2 - x1x2 - y1y2 - z1z2
//x = w1x2 + x1w2 + y1z2 - z1y2
//y = w1y2 + y1w2 + z1x2 - x1z2
//z = w1z2 + z1w2 + x1y2 - y1x2
// Remember the w coordinate is last!
vec4 qconcat3(vec4 a, vec4 b)
{
    return vec4(
        a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x,
        a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z
        );
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;

    // Position
    vec4 p = positions[index];
    vec3 v = velocities[index].xyz;
    p.xyz += dt * v.xyz;

    // Boundary conditions
    if (p.x < 0.)
    {
        p.x = 0.;
        v = reflect(v, vec3(1.,0.,0.));
    }
    if (p.x > 1.)
    {
        p.x = 1.;
        v = reflect(v, vec3(-1.,0.,0.));
    }
    if (p.y < 0.)
    {
        p.y = 0.;
        v = reflect(v, vec3(0.,1.,0.));
    }
    if (p.y > 1.)
    {
        p.y = 1.;
        v = reflect(v, vec3(0.,-1.,0.));
    }
    if (p.z < 0.)
    {
        p.z = 0.;
        v = reflect(v, vec3(0.,0.,1.));
    }
    if (p.z > 1.)
    {
        p.z = 1.;
        v = reflect(v, vec3(0.,0.,-1.));
    }

    positions[index] = p;
    velocities[index].xyz = v;

    // Rotation
    vec4 q = orientations[index];
    q = normalize(qconcat3(q, dt * angularVelocities[index]));
    orientations[index] = q;
}
]]
function particles:timestep_instances(dt)
    if self.progs.step_instances == nil then
        self.progs.step_instances = sf.make_shader_from_source({
            compsrc = instance_timestep_comp_src,
            })
    end
    local prog = self.progs.step_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end

    local pvbo = self.vbos.inst_positions
    local ovbo = self.vbos.inst_orientations
    local vvbo = self.vbos.inst_velocities
    local avvbo = self.vbos.inst_angularvelocities
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numPoints"), self.numPoints)
    gl.glUniform1f(uni("dt"), dt)
    gl.glDispatchCompute(self.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end

function particles:init_per_instance_attributes()
    local prog = self.progs.color
    local sz = 4 * self.numPoints * ffi.sizeof('GLfloat') -- xyzw

    local insp_loc = gl.glGetAttribLocation(prog, "instancePosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)

    local inso_loc = gl.glGetAttribLocation(prog, "instanceOrientation")
    local iovbo = glIntv(0)
    gl.glGenBuffers(1, iovbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, iovbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(inso_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_orientations = iovbo
    gl.glVertexAttribDivisor(inso_loc, 1)
    gl.glEnableVertexAttribArray(inso_loc)

    -- Velocities and angular velocities are not drawn; used only by compute.
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    self.vbos.inst_velocities = vvbo

    -- Angular velocities are not drawn; used only by compute.
    local avvbo = glIntv(0)
    gl.glGenBuffers(1, avvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, avvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    self.vbos.inst_angularvelocities = avvbo
end

function particles:initGL()
    print('initing GL!!')
    self.progs.color = sf.make_shader_from_source({
        vsrc = per_vertex_color_vert,
        fsrc = per_vertex_color_frag,
        })

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)
    do
        self:init_single_instance_attributes()
        self:init_per_instance_attributes()
        self:randomize_instance_positions()
    end

    do
        local lv = {}
        local numLights = 1
        local numflts = numLights*(4*4) -- sizeof light struct with padding
        local ubo = glIntv(0)
        gl.glGenBuffers(1, ubo)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, ubo[0])
        gl.glBufferData(GL.GL_UNIFORM_BUFFER, 4*numflts, nil, GL.GL_DYNAMIC_DRAW)
        self.uniformbuf = ubo

        local prog = self.progs.color
        local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
        print("lb:",lightBlock)
        gl.glUniformBlockBinding(prog, lightBlock, 0)
        gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
        --gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)
    end

    gl.glBindVertexArray(0)
end

function particles:exitGL()
    for _,p in pairs(self.progs) do
        gl.glDeleteProgram(p)
    end
    self.progs = {}

    gl.glBindVertexArray(self.vao)
    do
        for _,v in pairs(self.vbos) do
            gl.glDeleteBuffers(1,v)
        end
        self.vbos = {}
    end
    gl.glBindVertexArray(0)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function particles:render_for_one_eye(view, proj)
    local prog = self.progs.color
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(uni("prmtx"), 1, GL.GL_FALSE, glFloatv(16, proj))
    
    local m = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}
    --for i=1,16 do m[i] = model[i] end
    local s = 10
    mm.glh_scale(m, s,s,s)
    mm.glh_translate(m, -.5, -.5, -.5)

    gl.glUniformMatrix4fv(uni("mmtx"), 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(uni("vmtx"), 1, GL.GL_FALSE, glFloatv(16, view))

    local sc = .3 / math.pow(self.numPoints,1/3)
    gl.glUniform1f(uni("instScale"), sc)

    do
        -- Flatten array of light values
        local ldata = {}
        for _,v in pairs(self.lightData) do
            for i=1,#v do
                table.insert(ldata, v[i])
            end
        end
        local lightArr = glFloatv(#ldata, ldata)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, self.uniformbuf[0])
        gl.glBufferSubData(GL.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightArr), lightArr)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

        local prog = self.progs.color
        local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
        gl.glUniformBlockBinding(prog, lightBlock, 0)
        gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
    end

    gl.glEnable(GL.GL_BLEND)

    gl.glBindVertexArray(self.vao)
    gl.glDrawElementsInstanced(GL.GL_TRIANGLES, self.numInstTris, GL.GL_UNSIGNED_INT, nil, self.numPoints)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function particles:timestep(absTime, dt)
    self:timestep_instances(dt+.000000001)

    do
        -- Animate lighting data
        local r = 3
        local speed = 2
        local time = absTime --self.time
        local x,y,z = r*math.sin(speed*time),5,r*math.cos(speed*time)
        local dir = {.5*-x,-y,.5*-z}
        mm.normalize(dir)
        --print(dir[1], dir[2], dir[3])
        self.lightData = {
            -- Light 1
            {
                x,y,z,0,
                dir[1], dir[2], dir[3],0,
                1,1,0,0,
                0,.75,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
            },
        }
    end
end

function particles:rescaleInstances()
    --print(self.numPoints)
    gl.glBindVertexArray(self.vao)
    do
        gl.glDeleteBuffers(1,self.vbos.inst_positions)
        gl.glDeleteBuffers(1,self.vbos.inst_orientations)
        gl.glDeleteBuffers(1,self.vbos.inst_velocities)
        gl.glDeleteBuffers(1,self.vbos.inst_angularvelocities)

        self:init_per_instance_attributes()
        self:randomize_instance_positions()
    end
    gl.glBindVertexArray(0)
end

function particles:keypressed(key, scancode, action, mods)
    if key == 45 then
        self.numPoints = self.numPoints / 2
        self:rescaleInstances()
    elseif key == 61 then
        self.numPoints = self.numPoints * 2
        self:rescaleInstances()
    end
end

function particles:getNumPoints() return self.numPoints end

return particles
