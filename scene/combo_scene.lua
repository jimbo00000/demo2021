--[[ combo_scene.lua

    Simply glues 2 scene together.
]]
combo_scene = {}

combo_scene.__index = combo_scene

function combo_scene.new(...)
    local self = setmetatable({}, combo_scene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local ShadertoyLib = require("scene.shadertoy")
local TextSceneLib = require("scene.flying_strings2")
local ParticleSceneLib = require("scene.particles")

function combo_scene:init()
    self.Shadertoy = ShadertoyLib.new()
    self.FontScene = TextSceneLib.new()
    self.ParticleScene = ParticleSceneLib.new()
	
	self.gridThick = 1
	self.sunStripeThick = 1
    self.skylineHeightScale = 1
end
function combo_scene:setDataDirectory(dir)
    --self.Shadertoy:setDataDirectory(dir)
    self.FontScene:setDataDirectory(dir)
    self.ParticleScene:setDataDirectory(dir)
    self.dataDir = dir
end

function combo_scene:initGL()
    self.Shadertoy:initGL()
    self.FontScene:initGL()
    self.ParticleScene:initGL()
end

function combo_scene:exitGL()
    self.Shadertoy:exitGL()
    self.FontScene:exitGL()
    self.ParticleScene:exitGL()
end

function combo_scene:render_for_one_eye(view, proj)

	self.Shadertoy.gridThick = self.gridThick
	self.Shadertoy.sunStripeThick = self.sunStripeThick
	self.Shadertoy.skylineHeightScale = self.skylineHeightScale

    self.Shadertoy:render_for_one_eye(view, proj)
    self.FontScene:render_for_one_eye(view, proj)
    gl.glFrontFace(GL.GL_CCW)
    gl.glEnable(GL.GL_DEPTH_TEST)
    self.ParticleScene:render_for_one_eye(view, proj)
end

function combo_scene:timestep(absTime, dt)
    self.Shadertoy:timestep(absTime, dt)
    self.ParticleScene:timestep(absTime, dt)
    self.FontScene:timestep(absTime, dt)
end

return combo_scene
