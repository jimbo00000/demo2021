-- flying_strings2.lua

require("util.glfont")
local mm = require("util.matrixmath")

flying_strings2 = {}
flying_strings2.__index = flying_strings2


function flying_strings2.new(...)
    local self = setmetatable({}, flying_strings2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function flying_strings2:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.dataDir = nil
    self.glfont = nil
    self.towardsCenter = 0
    self.textFlatten = 1
    self.textscroll = 0
    self.strings = {}

    self.phase = 0 -- To fit with contained scene's keyframe name
    self.npts = 1000 -- To fit with contained scene's keyframe name
    self.colorSaturation = 0
    self.strings = {
        "Greetings to @party attendees",
        "Thanks Dr. Claw and @party team!",
    }

    self.strings_in_space = {
        {"Greetings @party attendees!", {-15.9, 2.3,0}, 0},
        {"Good to see you in real life again",  {2.9, .5, 0}, 0},
        {"Hope you and yours are well", {33.9,3.,0}, 0},
        {"Thanks lazyllama for the sweet soundtrack", {60,.0,0}, 0},
        {"And huge thanks to Dr. Claw for keeping the North American Demoscene alive", {90,2,0}, 0},
        {"Support open source, floss your teeth, and as always...", {120,0,0}, 0},
        {"Buy Dr.Claw a beer!!!", {170,1.5,0}, 1},
        {"keep coding", {240,1,0}, 0},
        --{"Buy Dr.Claw a beer", {-.9,.7,0}, 0},
    }
    self.flashing_strings_in_space = {
        {"Buy", {-1.2,.7,0}, 0},
        {"Dr.", {-.5,.7,0}, 0},
        {"Claw", {.1,.7,0}, 0},
        {"a", {0.9,.7,0}, 0},
        {"beer", {1.3,.7,0}, 0},
    }
end

function flying_strings2:setDataDirectory(dir)
    self.dataDir = dir
end

function flying_strings2:initGL()
    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('amiga_forever.fnt', 'amiga_forever_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function flying_strings2:exitGL()
    self.glfont:exitGL()
end

function flying_strings2:render_for_one_eye(view, proj)
    local col = {0.97, 0.46, 0.3}

    local mbird = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}
    --#mm.glh_translate(mbird, 0, -.2,0)
    --#mm.glh_rotate(mbird, 80, 1,0,0)
    --mm.glh_rotate(mbird, 30, 0,0,1)
    gl.glClear(GL.GL_DEPTH_BUFFER_BIT)

    for _,v in pairs(self.strings_in_space) do
        local m = {}
        for i=1,16 do m[i] = mbird[i] end
        local s = .02
        local tx = {}
        for i=1,3 do tx[i] = v[2][i] end
        local a = 0 --self.towardsCenter + v[3]
       -- a = a - math.floor(a)
 	    s = s + v[3] * .02
        local center = {-2,-2,-10}
        for i=1,3 do
            tx[i] = a*center[i] + (1-a)*tx[i]
        end
        mm.glh_translate(m, tx[1]+self.textscroll, tx[2], tx[3])
        mm.glh_scale(m, s, -s, s)
        mm.pre_multiply(m, view)
        self.glfont:render_string(m, proj, col, v[1])
    end

    -- Hijacking this variable for 'which string to display'
    if false then --self.textFlatten > 0 then
        local sis = self.flashing_strings_in_space[math.floor(self.textFlatten)]
        if sis then
            local v = sis
            local m = {}
        for i=1,16 do m[i] = mbird[i] end
            local s = .004
            local tx = {}
            for i=1,3 do tx[i] = v[2][i] end
            local a = self.towardsCenter + v[3]
            a = a - math.floor(a)
            local center = {-2,-2,-10}
            for i=1,3 do
                tx[i] = a*center[i] + (1-a)*tx[i]
            end
            mm.glh_translate(m, tx[1], tx[2], tx[3])
            mm.glh_scale(m, s, -s, s)
            mm.pre_multiply(m, view)
            self.glfont:render_string(m, proj, col, v[1])
        end
    end

end

function flying_strings2:timestep(absTime, dt)
    local t = .4 * absTime
    --self.towardsCenter = t - math.floor(t)
	self.textscroll = 80 - 3.*absTime
end

return flying_strings2
