from mido import MidiFile
import sys


rocketHeader = '''<?xml version="1.0" encoding="utf-8"?>
<rootElement>
<tracks rows="10000" startRow="0" endRow="10000" rowsPerBeat="8" beatsPerMin="{0}">
'''

rocketFooter = '''
	</tracks>
	</rootElement>
'''

rocketTrack = '''	<track name="{0}" folded="0" muteKeyCount="0" color="ffb27474">'''

rocketTrackFoot = '''
	</track>
'''

rocketKey = '''		<key row="{0}" value="{1}" interpolation="{2}" />'''


def dumpTrack(track, outfile):
	totalTime = 0
	onOffDict = {}
	lastRow = -1
	for msg in track:
		print(msg)
		if not msg.is_meta:

			# Keep track of state
			totalTime += msg.time
			row = int(totalTime / 11.625)
			value = msg.velocity
			interp = 3

			#print(msg.type)
			#print(dir(msg))
			onOffDict[msg.note] = msg.type
			#print(onOffDict)
			if msg.type == 'note_on':
				value = 1.0 * (msg.note-64)
			#else:
			#	value = 0.0

			preKfs = False #True

			print(msg.time, totalTime)
			if lastRow != totalTime:
				if preKfs:
					offval = 0.0
					skip = 0
					print(rocketKey.format(row-1, offval, skip), file=outfile)
				print(rocketKey.format(row, value, interp), file=outfile)
				lastRow = totalTime


def ReadMidi(filename):
	mid = MidiFile(filename)
	#print(mid)

	bpm = 137
	outname = 'midikfs.rocket'
	with open(outname, 'w') as f:
		print(rocketHeader.format(bpm), file=f)

		trackname = 'tri:rot' #'leadsynth'
		print(rocketTrack.format(trackname), file=f)
		for i, track in enumerate(mid.tracks):
			#print('Track {}: {}'.format(i, track.name))
			dumpTrack(track, f)
		print(rocketTrackFoot, file=f)
		
		print(rocketFooter, file=f)



def main(argv, argc):
	print(argv, argc)
	print("read")
	filename = 'lead_synth.mid' #'bassline.mid'
	if argc >= 2:
		filename = argv[1]
	ReadMidi(filename)

if __name__ == '__main__':
	main(sys.argv, len(sys.argv))
