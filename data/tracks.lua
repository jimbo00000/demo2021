tracks = {
  [1] = {
  ["name"] = "Scene",
  ["keys"] = {
  [0] = {
  ["interp"] = 0,
  ["row"] = 0,
  ["val"] = 3,
}
,
  [296] = {
  ["interp"] = 0,
  ["row"] = 296,
  ["val"] = 5,
}
,
}
,
}
,
  [2] = {
  ["name"] = "cam1:panx",
  ["keys"] = {
  [0] = {
  ["interp"] = 0,
  ["row"] = 0,
  ["val"] = 0,
}
,
}
,
}
,
  [3] = {
  ["name"] = "cam1:pany",
  ["keys"] = {
  [0] = {
  ["interp"] = 0,
  ["row"] = 0,
  ["val"] = 1,
}
,
}
,
}
,
  [4] = {
  ["name"] = "cam1:panz",
  ["keys"] = {
  [0] = {
  ["interp"] = 1,
  ["row"] = 0,
  ["val"] = -3,
}
,
  [56] = {
  ["interp"] = 3,
  ["row"] = 56,
  ["val"] = -3,
}
,
  [112] = {
  ["interp"] = 1,
  ["row"] = 112,
  ["val"] = 12,
}
,
  [297] = {
  ["interp"] = 1,
  ["row"] = 297,
  ["val"] = 1,
}
,
  [1168] = {
  ["interp"] = 1,
  ["row"] = 1168,
  ["val"] = 8,
}
,
}
,
}
,
  [5] = {
  ["name"] = "cam1:rotx",
  ["keys"] = {
}
,
}
,
  [6] = {
  ["name"] = "cam1:roty",
  ["keys"] = {
  [0] = {
  ["interp"] = 1,
  ["row"] = 0,
  ["val"] = -90,
}
,
  [112] = {
  ["interp"] = 1,
  ["row"] = 112,
  ["val"] = -90,
}
,
  [280] = {
  ["interp"] = 1,
  ["row"] = 280,
  ["val"] = 0,
}
,
  [264] = {
  ["interp"] = 1,
  ["row"] = 264,
  ["val"] = -90,
}
,
}
,
}
,
  [7] = {
  ["name"] = "cube:gridThick",
  ["keys"] = {
  [67] = {
  ["interp"] = 2,
  ["row"] = 67,
  ["val"] = 1,
}
,
  [122] = {
  ["interp"] = 3,
  ["row"] = 122,
  ["val"] = 1,
}
,
  [129] = {
  ["interp"] = 0,
  ["row"] = 129,
  ["val"] = 0,
}
,
  [91] = {
  ["interp"] = 3,
  ["row"] = 91,
  ["val"] = 0,
}
,
  [115] = {
  ["interp"] = 3,
  ["row"] = 115,
  ["val"] = 0,
}
,
  [108] = {
  ["interp"] = 3,
  ["row"] = 108,
  ["val"] = 1,
}
,
  [79] = {
  ["interp"] = 0,
  ["row"] = 79,
  ["val"] = 0,
}
,
  [22] = {
  ["interp"] = 0,
  ["row"] = 22,
  ["val"] = 1,
}
,
  [96] = {
  ["interp"] = 3,
  ["row"] = 96,
  ["val"] = 1,
}
,
  [101] = {
  ["interp"] = 3,
  ["row"] = 101,
  ["val"] = 0,
}
,
  [75] = {
  ["interp"] = 2,
  ["row"] = 75,
  ["val"] = 1,
}
,
  [71] = {
  ["interp"] = 2,
  ["row"] = 71,
  ["val"] = 0,
}
,
  [85] = {
  ["interp"] = 0,
  ["row"] = 85,
  ["val"] = 1,
}
,
  [63] = {
  ["interp"] = 2,
  ["row"] = 63,
  ["val"] = 0,
}
,
  [59] = {
  ["interp"] = 2,
  ["row"] = 59,
  ["val"] = 1,
}
,
}
,
}
,
  [8] = {
  ["name"] = "cube:rot",
  ["keys"] = {
}
,
}
,
  [9] = {
  ["name"] = "cube:skylineHeightScale",
  ["keys"] = {
}
,
}
,
  [10] = {
  ["name"] = "cube:sunStripeThick",
  ["keys"] = {
  [100] = {
  ["interp"] = 1,
  ["row"] = 100,
  ["val"] = 1,
}
,
  [86] = {
  ["interp"] = 1,
  ["row"] = 86,
  ["val"] = 0,
}
,
}
,
}
,
  [11] = {
  ["name"] = "tri:col",
  ["keys"] = {
}
,
}
,
  [12] = {
  ["name"] = "tri:posx",
  ["keys"] = {
}
,
}
,
  [13] = {
  ["name"] = "tri:posy",
  ["keys"] = {
}
,
}
,
  [14] = {
  ["name"] = "tri:rot",
  ["keys"] = {
  [0] = {
  ["interp"] = 0,
  ["row"] = 0,
  ["val"] = 240,
}
,
  [132] = {
  ["interp"] = 0,
  ["row"] = 132,
  ["val"] = 64,
}
,
  [99] = {
  ["interp"] = 0,
  ["row"] = 99,
  ["val"] = 64,
}
,
  [29] = {
  ["interp"] = 0,
  ["row"] = 29,
  ["val"] = 5,
}
,
  [45] = {
  ["interp"] = 0,
  ["row"] = 45,
  ["val"] = 90,
}
,
  [64] = {
  ["interp"] = 0,
  ["row"] = 64,
  ["val"] = 64,
}
,
  [123] = {
  ["interp"] = 0,
  ["row"] = 123,
  ["val"] = 64,
}
,
  [115] = {
  ["interp"] = 0,
  ["row"] = 115,
  ["val"] = 64,
}
,
  [90] = {
  ["interp"] = 0,
  ["row"] = 90,
  ["val"] = 64,
}
,
  [82] = {
  ["interp"] = 0,
  ["row"] = 82,
  ["val"] = 90,
}
,
  [74] = {
  ["interp"] = 0,
  ["row"] = 74,
  ["val"] = 90,
}
,
  [70] = {
  ["interp"] = 0,
  ["row"] = 70,
  ["val"] = 210,
}
,
  [100] = {
  ["interp"] = 0,
  ["row"] = 100,
  ["val"] = 1,
}
,
  [103] = {
  ["interp"] = 0,
  ["row"] = 103,
  ["val"] = 64,
}
,
  [28] = {
  ["interp"] = 0,
  ["row"] = 28,
  ["val"] = 64,
}
,
  [119] = {
  ["interp"] = 0,
  ["row"] = 119,
  ["val"] = 300,
}
,
  [94] = {
  ["interp"] = 0,
  ["row"] = 94,
  ["val"] = 210,
}
,
  [86] = {
  ["interp"] = 0,
  ["row"] = 86,
  ["val"] = 64,
}
,
  [128] = {
  ["interp"] = 0,
  ["row"] = 128,
  ["val"] = 300,
}
,
  [78] = {
  ["interp"] = 0,
  ["row"] = 78,
  ["val"] = 64,
}
,
  [66] = {
  ["interp"] = 0,
  ["row"] = 66,
  ["val"] = 240,
}
,
  [16] = {
  ["interp"] = 0,
  ["row"] = 16,
  ["val"] = 150,
}
,
  [111] = {
  ["interp"] = 0,
  ["row"] = 111,
  ["val"] = 300,
}
,
  [107] = {
  ["interp"] = 0,
  ["row"] = 107,
  ["val"] = 64,
}
,
}
,
}
,
}
return tracks